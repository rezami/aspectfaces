/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.descriptors.jpa;

import java.util.ArrayList;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.VariableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

public class ColumnAnnotationDescriptor implements AnnotationDescriptor, VariableJoinPoint {

	@Override
	public String getAnnotationName() {
		return "javax.persistence.Column";
	}

	@Override
	public List<Variable> getVariables(AnnotationProvider annotationProvider) {
		List<Variable> variables = new ArrayList<Variable>();
		Boolean nullable = (Boolean) annotationProvider.getValue("nullable");
		if (nullable == null || nullable == false) {
			// set only if true, false is default
			variables.add(new Variable("notNull", "true", false));
			variables.add(new Variable("required", true));
		}
		variables.add(new Variable("maxLength", annotationProvider.getValue("length")));
		variables.add(new Variable("unique", annotationProvider.getValue("unique")));
		variables.add(new Variable("precision", annotationProvider.getValue("precision")));
		variables.add(new Variable("scale", annotationProvider.getValue("scale")));
		return variables;
	}
}
