/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.ondemand;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.view.facelets.ComponentConfig;

import com.codingcrayons.aspectfaces.exceptions.AFException;

import com.sun.faces.facelets.impl.GeneratorHandler;

/**
 * Dynamic Default AF assembler to Facelets
 */
public class DefaultAFGeneratorHandler extends AFGeneratorHandler {

	public DefaultAFGeneratorHandler(ComponentConfig config) {
		this(config, -1);
	}

	public DefaultAFGeneratorHandler(ComponentConfig config, int afCacheTime) {
		super(config, afCacheTime);
	}

	/**
	 * Template method that adds content to the fragment (non-Javadoc)
	 *
	 * @see GeneratorHandler#hookInputStreamToApply()
	 */
	@Override
	protected InputStream hookInputStreamToApply() {
		try {
			initializeAF();
			if (classesToInspect == null || classesToInspect.length == 0) {
				// if nothing to add then ingnore it
				return new ByteArrayInputStream((EMPTY_HEAD + EMPTY_TAIL).getBytes());
			}
			return hookCustomInputStream();
		} catch (AFException e) {
			return viewFragmentExceptionIS(e);
		}
	}

	/**
	 * Template method that adds content to the fragment (non-Javadoc)
	 *
	 * @see GeneratorHandler#hookInputStreamToApply()
	 */
	protected InputStream hookCustomInputStream() {
		return generateIS(classesToInspect, getConfig(), fetchLayout());
	}

	private InputStream generateIS(Class<?>[] clazz, String configName, String layout) {
		return generateAsIS(clazz, configName, new String[]{}, new ArrayList<String>(), layout);
	}

	private String fetchLayout() {
		String overriddenLayout = null;
		if (layout != null) {
			FacesContext ctx = FacesContext.getCurrentInstance();
			overriddenLayout = (String) executeExpressionInElContext(ctx.getApplication().getExpressionFactory(), ctx.getELContext(), layout.getValue());
		}
		return overriddenLayout == null ? null : "../../layouts/" + overriddenLayout;
	}
}
