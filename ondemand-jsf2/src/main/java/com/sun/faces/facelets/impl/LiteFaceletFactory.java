/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sun.faces.facelets.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.el.ELException;
import javax.el.ExpressionFactory;
import javax.faces.FacesException;
import javax.faces.view.facelets.FaceletCache;
import javax.faces.view.facelets.FaceletHandler;
import javax.faces.view.facelets.ResourceResolver;

import com.sun.faces.facelets.Facelet;
import com.sun.faces.facelets.LiteCache;
import com.sun.faces.facelets.compiler.Compiler;
import com.sun.faces.facelets.compiler.LiteCompiler;
import com.sun.faces.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default FaceletFactory implementation.
 *
 * @author Jacob Hookom
 * @version $Id: GlueFaceletFactory.java,v 1.10 2007/04/09 01:13:17 youngm Exp $
 *          <p/>
 *          Revision 1.2.  March 14th 2013
 */
public final class LiteFaceletFactory extends DefaultFaceletFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(LiteFaceletFactory.class);

	private final Compiler compiler;

	private Map<String, URL> relativeLocations;

	private final URL baseUrl;

	public LiteFaceletFactory(Compiler compiler, ResourceResolver resolver) throws IOException {
		this(compiler, resolver, -1);
	}

	public LiteFaceletFactory(Compiler compiler, ResourceResolver resolver, long refreshPeriod) {
		this(compiler, resolver, refreshPeriod, null);
	}

	public LiteFaceletFactory(Compiler compiler, ResourceResolver resolver, long refreshPeriod, FaceletCache<?> cache) {
		super(compiler, resolver, refreshPeriod, cache);
		Util.notNull("compiler", compiler);
		this.compiler = compiler;
		this.relativeLocations = new ConcurrentHashMap<String, URL>();
		this.baseUrl = resolver.resolveUrl("/");
	}

	public Facelet getFacelet(InputStream is) throws IOException, FacesException, ELException {
		// Simplified version
		URL url = resolveURL("/");
		return this.createFacelet(is, url);
	}

	public Facelet getFacelet(InputStream is, String alias) throws IOException, FacesException, ELException {
		return this.createFacelet(is, alias);
	}

	private Facelet createFacelet(InputStream is, URL url) throws IOException, FacesException, ELException {
		LOGGER.trace("Creating Facelet for: '{}'.", url);
		String alias = "/" + url.getFile().replaceFirst(this.baseUrl.getFile(), "");

		try {
			FaceletHandler h = ((LiteCompiler) this.compiler).compile(is, url, alias);
			return createDefaultFacelet(url, alias, h);
			//return new DefaultFacelet(this, this.compiler.createExpressionFactory(), url, alias, h);
		} catch (FileNotFoundException fnfe) {
			LOGGER.warn("{} not found at {}", alias, url.toExternalForm());
			throw new FileNotFoundException("Facelet Not Found: ");
		}
	}

	private Facelet createFacelet(InputStream is, String alias) throws IOException, FacesException, ELException {
		LOGGER.trace("Creating Facelet for: {}", alias);
		URL url = resolveURL("/");

		try {
			FaceletHandler h = ((LiteCompiler) this.compiler).compile(is, url, alias);

			return createDefaultFacelet(url, alias, h);
			//return new DefaultFacelet(this, this.compiler.createExpressionFactory(), url, alias, h);
		} catch (FileNotFoundException fnfe) {
			throw new FileNotFoundException("Facelet Not Found: " + alias);
		}
	}

	private Facelet createDefaultFacelet(URL url, String alias, FaceletHandler h) throws IOException {
		try {
			Constructor<?> constructor = LiteCache.getConstructor("com.sun.faces.facelets.impl.DefaultFacelet");
			if (constructor == null) {
				Class<?> cm = Class.forName("com.sun.faces.facelets.impl.DefaultFacelet");
				Class<?> partypes[] = {DefaultFaceletFactory.class, ExpressionFactory.class, URL.class, String.class, FaceletHandler.class};

				constructor = cm.getConstructor(partypes);
				constructor.setAccessible(true);
				LiteCache.putConstructor("com.sun.faces.facelets.impl.DefaultFacelet", constructor);
			}

			Object arglist[] = {this, this.compiler.createExpressionFactory(), url, alias, h};
			return (Facelet) constructor.newInstance(arglist);
		} catch (Exception e) {
			throw new IOException("DefaultFacelet not found.");
		}
	}

	private URL resolveURL(String uri) throws IOException {

		URL url = this.relativeLocations.get(uri);
		if (url == null) {
			url = this.resolveURL(this.baseUrl, uri);
			if (url != null) {
				this.relativeLocations.put(uri, url);
			} else {
				throw new IOException("'" + uri + "' not found.");
			}
		}
		return url;
	}

	/**
	 * Compiler this factory uses
	 *
	 * @return final Compiler instance
	 */
	public Compiler getCompiler() {
		return this.compiler;
	}
}
