/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sun.faces.facelets;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class LiteCache {

	private static Map<String, Constructor<?>> cacheConstructors = new HashMap<String, Constructor<?>>();
	private static Map<String, Method> cacheMethods = new HashMap<String, Method>();
	private static Map<String, Field> cacheFields = new HashMap<String, Field>();
	private static Map<String, Class<?>> cacheClasses = new HashMap<String, Class<?>>();

	public static Constructor<?> getConstructor(String key) {
		return cacheConstructors.get(key);
	}

	public static void putConstructor(String key, Constructor<?> c) {
		cacheConstructors.put(key, c);
	}

	public static Method getMethod(String key) {
		return cacheMethods.get(key);
	}

	public static void putMethod(String key, Method m) {
		cacheMethods.put(key, m);
	}

	public static Field getField(String key) {
		return cacheFields.get(key);
	}

	public static void putField(String key, Field m) {
		cacheFields.put(key, m);
	}

	public static Class<?> getClass(String key) {
		return cacheClasses.get(key);
	}

	public static void putClass(String key, Class<?> m) {
		cacheClasses.put(key, m);
	}
}
