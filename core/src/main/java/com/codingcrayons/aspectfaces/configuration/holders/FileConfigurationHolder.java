/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration.holders;

import java.io.File;
import java.util.Calendar;

import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.parsers.ConfigurationParser;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;

/**
 * @author Vaclav Chalupa (vac.chalupa@gmail.com)
 * @version 1.0
 */
public class FileConfigurationHolder extends ConfigurationHolder {

	private final File file;

	public FileConfigurationHolder(Configuration configuration, File file, boolean lazyInitialize,
								   boolean checkModification) throws ConfigurationParsingException, ConfigurationFileNotFoundException {

		this(configuration, file, lazyInitialize, checkModification, null);
	}

	public FileConfigurationHolder(Configuration configuration, File file, boolean lazyInitialize,
								   boolean checkModification, ConfigurationParser parser)
		throws ConfigurationParsingException, ConfigurationFileNotFoundException {

		super(configuration, checkModification, parser);
		this.file = file;
		if (!lazyInitialize) {
			this.loadConfiguration();
		}
	}

	@Override
	protected boolean configurationFileModified() {
		return this.file.lastModified() > this.loadedTime;
	}

	@Override
	protected final void loadConfiguration() throws ConfigurationParsingException, ConfigurationFileNotFoundException {
		this.loadedTime = Calendar.getInstance().getTime().getTime();
		this.getParser().parseConfiguration(file, configuration);
		this.configuration.setLoaded(true);
	}
}
