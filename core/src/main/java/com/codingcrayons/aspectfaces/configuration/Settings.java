/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.File;

import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.util.Strings;

public class Settings {

	private static final String DEFAULT_REAL_PATH = null;
	private static final String DEFAULT_HEADER_FILE = null;
	private static final String DEFAULT_FOOTER_FILE = null;
	private static final String DEFAULT_GALLERY_DIRECTORY = File.separator + "aspectfaces-gallery";
	private static final String DEFAULT_EVALUATOR_CLASS = null;

	private static String defaultRealPath = DEFAULT_REAL_PATH;
	private static String defaultHeaderFile = DEFAULT_HEADER_FILE;
	private static String defaultFooterFile = DEFAULT_FOOTER_FILE;
	private static String defaultGalleryDirectory = DEFAULT_GALLERY_DIRECTORY;
	private static String defaultEvaluatorClass = DEFAULT_EVALUATOR_CLASS;

	public static void setDefaultRealPath(String realPath) {
		defaultRealPath = realPath;
	}

	public static void setDefaultHeaderFile(String headerFile) {
		defaultHeaderFile = headerFile;
	}

	public static void setDefaultFooterFile(String footerFile) {
		defaultFooterFile = footerFile;
	}

	public static void setDefaultGalleryDirectory(String galleryDirectory) {
		defaultGalleryDirectory = galleryDirectory;
	}

	public static void setDefaultEvaluatorClass(String evaluatorClass) {
		defaultEvaluatorClass = evaluatorClass;
	}

	public static void reset() {
		defaultRealPath = DEFAULT_REAL_PATH;
		defaultHeaderFile = DEFAULT_HEADER_FILE;
		defaultFooterFile = DEFAULT_FOOTER_FILE;
		defaultGalleryDirectory = DEFAULT_GALLERY_DIRECTORY;
		defaultEvaluatorClass = DEFAULT_EVALUATOR_CLASS;
	}

	private String realPath;
	private String galleryDirectory;
	private String headerFile;
	private String footerFile;
	private String evaluatorClassName;

	public Settings() {
		this.realPath = defaultRealPath;
		this.galleryDirectory = defaultGalleryDirectory;
		this.headerFile = defaultHeaderFile;
		this.footerFile = defaultFooterFile;
		this.evaluatorClassName = defaultEvaluatorClass;
	}

	public Settings(String realPath, String galleryDirectory, String headerFile, String footerFile,
					String evaluatorClassName) {
		this.realPath = realPath;
		this.galleryDirectory = galleryDirectory;
		this.headerFile = headerFile;
		this.footerFile = footerFile;
		this.evaluatorClassName = evaluatorClassName;
	}

	public String getRealPath() {
		return realPath;
	}

	public void setRealPath(String realPath) {
		this.realPath = realPath;
	}

	public String getGalleryDirectory() {
		return galleryDirectory;
	}

	public void setGalleryDirectory(String galleryDirectory) {
		this.galleryDirectory = galleryDirectory;
	}

	public String getHeaderFile() throws TemplateFileNotFoundException {
		if (Strings.isBlank(headerFile)) {
			throw new TemplateFileNotFoundException("Header template file not set.");
		}
		return headerFile;
	}

	public void setHeaderFile(String headerFile) {
		this.headerFile = headerFile;
	}

	public String getFooterFile() throws TemplateFileNotFoundException {
		if (Strings.isBlank(footerFile)) {
			throw new TemplateFileNotFoundException("Footer template file not set.");
		}
		return footerFile;
	}

	public void setFooterFile(String footerFile) {
		this.footerFile = footerFile;
	}

	public String getEvaluatorClassName() {
		return evaluatorClassName;
	}

	public void setEvaluatorClassName(String evaluatorClassName) {
		this.evaluatorClassName = evaluatorClassName;
	}
}
