/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.codingcrayons.aspectfaces.annotation.registration.AnnotationContainer;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor;
import com.codingcrayons.aspectfaces.cache.CacheProvider;
import com.codingcrayons.aspectfaces.composition.UIFragmentComposer;
import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.ConfigurationStorage;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.configuration.Settings;
import com.codingcrayons.aspectfaces.configuration.StaticConfiguration;
import com.codingcrayons.aspectfaces.exceptions.AnnotationDescriptorNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotRegisteredException;
import com.codingcrayons.aspectfaces.exceptions.CacheProviderNotSetException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationNotSetException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;
import com.codingcrayons.aspectfaces.exceptions.InspectionException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.metamodel.Inspector;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.properties.PropertyLoader;
import com.codingcrayons.aspectfaces.util.Files;
import com.codingcrayons.aspectfaces.variableResolver.TagParserException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AFWeaver implements Serializable {

	private static final long serialVersionUID = 5193577150807524293L;

	private static final Logger LOGGER = LoggerFactory.getLogger(AFWeaver.class);
	private static final String PROPERTIES_FILE = "/aspectfaces.properties";
	private static final String DEFAULT_VARIABLE_IDENTIFIER = "$";
	private static final String DEFAULT_INSTANCE_VARIABLE_PREFIX = "i";

	private static String openVariableBoundaryIdentifier = DEFAULT_VARIABLE_IDENTIFIER;
	private static String closeVariableBoundaryIdentifier = DEFAULT_VARIABLE_IDENTIFIER;
	private static String instanceVariablePrefix = DEFAULT_INSTANCE_VARIABLE_PREFIX;
	private static CacheProvider defaultCacheProvider;

	private Configuration configuration;
	private Inspector inspector;
	private UIFragmentComposer fragmentComposer;
	private CacheProvider cacheProvider;

	/**
	 * Loads properties and configure default settings and values.
	 *
	 * @throws ConfigurationFileNotFoundException, ConfigurationParsingException
	 */
	public static void init() throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		LOGGER.trace("Loading properties from {}.", PROPERTIES_FILE);
		PropertyLoader loader = new PropertyLoader(PROPERTIES_FILE);
		AFWeaver.init(loader);
	}

	/**
	 * Loads properties from the specified input stream and configure default settings and values.
	 *
	 * @param input - input stream of properties file
	 * @throws ConfigurationFileNotFoundException, ConfigurationParsingException
	 */
	public static void init(InputStream input) throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		LOGGER.trace("Loading properties from input stream");
		PropertyLoader loader = new PropertyLoader(input);
		AFWeaver.init(loader);
	}

	/**
	 * Loads properties from the specified loader and configure default settings and values.
	 *
	 * @param loader - specified properties file
	 */
	private static void init(PropertyLoader loader) {
		// AFWeaver
		String variableIdentifier = loader.getProperty("template-variable-identifier");
		if (variableIdentifier != null) {
			AFWeaver.setOpenVariableBoundaryIdentifier(variableIdentifier);
			AFWeaver.setCloseVariableBoundaryIdentifier(variableIdentifier);
		}
		String openVariableIdentifier = loader.getProperty("open-template-variable-identifier");
		if (openVariableIdentifier != null) {
			AFWeaver.setOpenVariableBoundaryIdentifier(openVariableIdentifier);
		}
		String closeVariableIdentifier = loader.getProperty("close-template-variable-identifier");
		if (closeVariableIdentifier != null) {
			AFWeaver.setCloseVariableBoundaryIdentifier(closeVariableIdentifier);
		}
		String instanceVariablePrefix = loader.getProperty("instance-variable-prefix");
		if (instanceVariablePrefix != null) {
			AFWeaver.setInstanceVariablePrefix(instanceVariablePrefix);
		}

		// Configuration
		String ignoreFields = loader.getProperty("ignore-fields");
		if (ignoreFields != null) {
			for (String name : AFWeaver.parseArrayProperty(ignoreFields)) {
				Configuration.addDefaultIgnoreField(name);
			}
		}
		String ignoringAnnotations = loader.getProperty("ignoring-annotations");
		if (ignoringAnnotations != null) {
			for (String name : AFWeaver.parseArrayProperty(ignoringAnnotations)) {
				Configuration.addDefaultIgnoringAnnotation(name);
			}
		}

		// Settings
		String realPath = loader.getProperty("real-path");
		if (realPath != null) {
			Settings.setDefaultRealPath(realPath);
		}
		String header = loader.getProperty("header");
		if (header != null) {
			Settings.setDefaultHeaderFile(header);
		}
		String footer = loader.getProperty("footer");
		if (footer != null) {
			Settings.setDefaultFooterFile(footer);
		}
		String gallery = loader.getProperty("gallery");
		if (gallery != null) {
			Settings.setDefaultGalleryDirectory(gallery);
		}
		String evaluator = loader.getProperty("evaluator");
		if (evaluator != null) {
			Settings.setDefaultEvaluatorClass(evaluator);
		}

		// Context
		String orderAnnotation = loader.getProperty("order-annotation");
		if (orderAnnotation != null) {
			Context.setDefaultOrderAnnotation(orderAnnotation);
		}
		String profilesAnnotation = loader.getProperty("profiles-annotation");
		if (profilesAnnotation != null) {
			Context.setDefaultProfilesAnnotation(profilesAnnotation);
		}
		String userRolesAnnotation = loader.getProperty("user-roles-annotation");
		if (userRolesAnnotation != null) {
			Context.setDefaultUserRolesAnnotation(userRolesAnnotation);
		}
	}

	private static List<String> parseArrayProperty(String propertyValue) {
		StringTokenizer tokenizer = new StringTokenizer(propertyValue);
		List<String> tokens = new ArrayList<String>();
		while (tokenizer.hasMoreTokens()) {
			tokens.add(tokenizer.nextToken());
		}
		return tokens;
	}

	public static void reset() {
		setOpenVariableBoundaryIdentifier(DEFAULT_VARIABLE_IDENTIFIER);
		setCloseVariableBoundaryIdentifier(DEFAULT_VARIABLE_IDENTIFIER);
		Settings.reset();
		Context.reset();
		Configuration.reset();
	}

	/**
	 * Gets global AFWeaver cache provider which is being automatically set to AF instances.
	 *
	 * @return global AFWeaver cache provider
	 */
	public static CacheProvider getDefaultCacheProvider() {
		return AFWeaver.defaultCacheProvider;
	}

	/**
	 * Sets global AFWeaver cache provider which is being automatically set to AF instances.
	 *
	 * @param globalCacheProvider - cache provider
	 */
	public static void setDefaultCacheProvider(CacheProvider globalCacheProvider) {
		AFWeaver.defaultCacheProvider = globalCacheProvider;
	}

	public static void setOpenVariableBoundaryIdentifier(String identifier) {
		AFWeaver.openVariableBoundaryIdentifier = identifier;
	}

	public static String getOpenVariableBoundaryIdentifier() {
		return openVariableBoundaryIdentifier;
	}

	public static void setCloseVariableBoundaryIdentifier(String identifier) {
		AFWeaver.closeVariableBoundaryIdentifier = identifier;
	}

	public static String getCloseVariableBoundaryIdentifier() {
		return closeVariableBoundaryIdentifier;
	}

	/**
	 * @return instance variable prefix used in gallery tags
	 */
	public static String getInstanceVariablePrefix() {
		return instanceVariablePrefix;
	}

	/**
	 * Sets the specified prefix to instance variable in gallery tags.
	 *
	 * @param prefix
	 */
	public static void setInstanceVariablePrefix(String prefix) {
		AFWeaver.instanceVariablePrefix = prefix;
	}

	/**
	 * Register annotations by all included annotation descriptors
	 *
	 * @throws AnnotationDescriptorNotFoundException if can not instantiate a descriptor; or a constructor is not
	 *                                               accessible; or descriptor's class could not be found
	 */
	public static void registerAllAnnotations() throws AnnotationDescriptorNotFoundException {

		try {
			for (Class<?> clazz : Files.getClasses("com.codingcrayons.aspectfaces.annotation.descriptors")) {
				boolean dummyChecker = false;
				for (Class<?> cinterface : clazz.getInterfaces()) {
					if (cinterface.getName().equals(
						"com.codingcrayons.aspectfaces.annotation.registration.AnnotationDescriptor")) {
						dummyChecker = true;
					}
				}
				if (!dummyChecker) {
					continue;
				}
				@SuppressWarnings("unchecked")
				Class<AnnotationDescriptor> fieldDescriptorClass = (Class<AnnotationDescriptor>) clazz;
				try {
					AnnotationContainer.getInstance().registerAnnotation(fieldDescriptorClass.newInstance());
				} catch (InstantiationException e) {
					throw new AnnotationDescriptorNotFoundException("Can't instantiate "
						+ fieldDescriptorClass.getName(), e);
				} catch (IllegalAccessException e) {
					throw new AnnotationDescriptorNotFoundException("A constructor " + fieldDescriptorClass.getName()
						+ " is not accessible", e);
				}
			}
		} catch (ClassNotFoundException e) {
			throw new AnnotationDescriptorNotFoundException(e);
		} catch (IOException e) {
			throw new AnnotationDescriptorNotFoundException(e);
		}
	}

	/**
	 * Adds configuration by name
	 *
	 * @param config
	 * @param file
	 * @param lazyInitialize
	 * @param checkModification
	 * @throws ConfigurationParsingException      if the given configuration has not valid format; or wraps DocumentException
	 * @throws ConfigurationFileNotFoundException if the given file could not be found
	 */
	public static void addConfiguration(Configuration config, File file, boolean lazyInitialize,
										boolean checkModification) throws ConfigurationParsingException, ConfigurationFileNotFoundException {

		ConfigurationStorage.getInstance().addConfiguration(config, file, lazyInitialize, checkModification);
	}

	/**
	 * Adds a static configuration by a file name with checking modification and without lazy initialization.
	 *
	 * @param filePath
	 * @throws ConfigurationParsingException      if the given configuration has not valid format; or wraps DocumentException
	 * @throws ConfigurationFileNotFoundException if the given file could not be found
	 */
	public static void addStaticConfiguration(String filePath) throws ConfigurationParsingException,
		ConfigurationFileNotFoundException {

		File config = new File(filePath);
		addConfiguration(new StaticConfiguration(config.getName()), config, false, true);
	}

	/**
	 * Adds configuration by name
	 *
	 * @param config
	 * @param url
	 * @param lazyInitialize
	 * @param checkModification
	 * @throws ConfigurationParsingException      if the given configuration has not valid format; or wraps DocumentException
	 * @throws ConfigurationFileNotFoundException if the given file could not be found
	 */
	public static void addConfiguration(Configuration config, URL url, boolean lazyInitialize, boolean checkModification)
		throws ConfigurationFileNotFoundException, ConfigurationParsingException {

		ConfigurationStorage.getInstance().addConfiguration(config, url, lazyInitialize, checkModification);
	}

	/**
	 * Registers an annotation
	 *
	 * @param annotation
	 */
	public static void registerAnnotation(AnnotationDescriptor annotation) {
		AnnotationContainer.getInstance().registerAnnotation(annotation);
	}

	/**
	 * Sets up configuration to use
	 *
	 * @param configurationName
	 * @throws ConfigurationParsingException      if the given configuration has not valid format; or wraps DocumentException
	 * @throws ConfigurationFileNotFoundException if the given file could not be found
	 * @throws ConfigurationNotFoundException     if the configuration could not be found, perhaps is not loaded yet
	 */
	public AFWeaver(String configurationName) throws ConfigurationNotFoundException,
		ConfigurationFileNotFoundException, ConfigurationParsingException {

		this(ConfigurationStorage.getInstance().getConfiguration(configurationName), (Inspector) null);
	}

	public AFWeaver(Configuration configuration) {
		this(configuration, (Inspector) null);
	}

	public AFWeaver(Inspector inspector) {
		this((Configuration) null, inspector);
	}

	/**
	 * @param configurationName
	 * @param inspector
	 * @throws ConfigurationParsingException      if the given configuration has not valid format; or wraps DocumentException
	 * @throws ConfigurationFileNotFoundException if the given file could not be found
	 * @throws ConfigurationNotFoundException     if the configuration could not be found, perhaps is not loaded yet
	 */
	public AFWeaver(String configurationName, Inspector inspector) throws ConfigurationNotFoundException,
		ConfigurationFileNotFoundException, ConfigurationParsingException {

		this(ConfigurationStorage.getInstance().getConfiguration(configurationName), inspector);
	}

	public AFWeaver(Configuration configuration, Inspector inspector) {
		this.fragmentComposer = new UIFragmentComposer();
		this.configuration = configuration;
		this.inspector = inspector;
		this.cacheProvider = AFWeaver.defaultCacheProvider;
	}

	/**
	 * Set the specified cache provider to AFWeaver instance.
	 *
	 * @param cacheProvider to set in AFWeaver instance
	 */
	public void setCacheProvider(CacheProvider cacheProvider) {
		this.cacheProvider = cacheProvider;
	}

	/**
	 * Return cache provider of AFWeaver instance.
	 *
	 * @return cache provider
	 */
	public CacheProvider getCacheProvider() {
		return cacheProvider;
	}

	/**
	 * Gets configuration for generation.
	 *
	 * @return current configuration for generation
	 */
	public Configuration getConfiguration() {
		return this.configuration;
	}

	/**
	 * Sets configuration for generation.
	 *
	 * @param configurationName - name of configuration
	 * @throws ConfigurationParsingException      if the given configuration has not valid format; or wraps DocumentException
	 * @throws ConfigurationFileNotFoundException if the given file could not be found
	 * @throws ConfigurationNotFoundException     if the configuration could not be found, perhaps is not loaded yet
	 */
	public void setConfiguration(String configurationName) throws ConfigurationNotFoundException,
		ConfigurationFileNotFoundException, ConfigurationParsingException {
		this.configuration = ConfigurationStorage.getInstance().getConfiguration(configurationName);
	}

	/**
	 * Sets an entity inspector and resets old meta properties.
	 *
	 * @param inspector - inspector
	 */
	public void setInspector(Inspector inspector) {
		this.inspector = inspector;
		this.fragmentComposer = new UIFragmentComposer();
	}

	/**
	 * Sets configuration for generation.
	 *
	 * @param configuration - configuration
	 */
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Adds a meta property to generation.
	 *
	 * @param metaProperty - meta property
	 */
	public void addMetaProperty(MetaProperty metaProperty) {
		this.fragmentComposer.addField(metaProperty);
	}

	/**
	 * Generates a UI Fragment with default settings
	 *
	 * @param context - AF context
	 * @return UI Fragment as String
	 * @throws ConfigurationNotSetException     if a configuration is not set
	 * @throws TemplateFileNotFoundException    if a header or a footer file can't be found
	 * @throws InspectionException              if an inspection problem occurred, details in a message
	 * @throws AnnotationNotRegisteredException if an annotation is used, but isn't registered
	 * @throws EvaluatorException               if an evaluation problem occurred, details in a message
	 * @throws AnnotationNotFoundException      if an annotation could not be found, but it's used in a class
	 * @throws TemplateFileAccessException      if a header or a footer file is not accessible
	 * @throws CacheProviderNotSetException     if a cache provider is null
	 * @throws TagParserException
	 */
	public String generate(Context context) throws ConfigurationNotSetException, TemplateFileNotFoundException,
		InspectionException, AnnotationNotRegisteredException, EvaluatorException, AnnotationNotFoundException,
		TemplateFileAccessException, CacheProviderNotSetException, TagParserException {
		return generate(context, new Settings());
	}

	/**
	 * Tries to find generated UI Fragment in cache by the specified key and the default region. If a fragment is not
	 * found in cache, generation is being invoked and generated fragment is being put in the default cache region under
	 * the specified key.
	 *
	 * @param context - AF context
	 * @param key     - key in region
	 * @return UI fragment
	 * @throws ConfigurationNotSetException     if a configuration is not set
	 * @throws TemplateFileNotFoundException    if a header or a footer file can't be found
	 * @throws InspectionException              if an inspection problem occurred, details in a message
	 * @throws AnnotationNotRegisteredException if an annotation is used, but isn't registered
	 * @throws EvaluatorException               if an evaluation problem occurred, details in a message
	 * @throws AnnotationNotFoundException      if an annotation could not be found, but it's used in a class
	 * @throws TemplateFileAccessException      if a header or a footer file is not accessible
	 * @throws CacheProviderNotSetException     if a cache provider is null
	 * @throws TagParserException
	 */
	public String generate(Context context, String key) throws ConfigurationNotSetException,
		TemplateFileNotFoundException, InspectionException, AnnotationNotRegisteredException, EvaluatorException,
		AnnotationNotFoundException, TemplateFileAccessException, CacheProviderNotSetException, TagParserException {
		return this.generate(context, new Settings(), null, key);
	}

	/**
	 * Tries to find generated UI Fragment in cache by the specified key and the default region. If a fragment is not
	 * found in cache, generation is being invoked and generated fragment is being put in the default cache region under
	 * the specified key.
	 *
	 * @param context  - AF context
	 * @param settings - configuration settings
	 * @param key      - key in region
	 * @return UI fragment
	 * @throws ConfigurationNotSetException     if a configuration is not set
	 * @throws TemplateFileNotFoundException    if a header or a footer file can't be found
	 * @throws InspectionException              if an inspection problem occurred, details in a message
	 * @throws AnnotationNotRegisteredException if an annotation is used, but isn't registered
	 * @throws EvaluatorException               if an evaluation problem occurred, details in a message
	 * @throws AnnotationNotFoundException      if an annotation could not be found, but it's used in a class
	 * @throws TemplateFileAccessException      if a header or a footer file is not accessible
	 * @throws CacheProviderNotSetException     if a cache provider is null
	 * @throws TagParserException
	 */
	public String generate(Context context, Settings settings, String key) throws ConfigurationNotSetException,
		TemplateFileNotFoundException, InspectionException, AnnotationNotRegisteredException, EvaluatorException,
		AnnotationNotFoundException, TemplateFileAccessException, CacheProviderNotSetException, TagParserException {
		return this.generate(context, settings, null, key);
	}

	/**
	 * Tries to find generated UI Fragment in cache by the specified key and region. If a fragment is not found in
	 * cache, generation is being invoked and generated fragment is being put in the specified cache region under the
	 * specified key.
	 *
	 * @param context - AF context
	 * @param region  - cache region, if null, default region is being used
	 * @param key     - key in region
	 * @return UI fragment
	 * @throws ConfigurationNotSetException     if a configuration is not set
	 * @throws TemplateFileNotFoundException    if a header or a footer file can't be found
	 * @throws InspectionException              if an inspection problem occurred, details in a message
	 * @throws AnnotationNotRegisteredException if an annotation is used, but isn't registered
	 * @throws EvaluatorException               if an evaluation problem occurred, details in a message
	 * @throws AnnotationNotFoundException      if an annotation could not be found, but it's used in a class
	 * @throws TemplateFileAccessException      if a header or a footer file is not accessible
	 * @throws CacheProviderNotSetException     if a cache provider is null
	 * @throws TagParserException
	 */
	public String generate(Context context, String region, String key) throws ConfigurationNotSetException,
		TemplateFileNotFoundException, InspectionException, AnnotationNotRegisteredException, EvaluatorException,
		AnnotationNotFoundException, TemplateFileAccessException, CacheProviderNotSetException, TagParserException {
		return this.generate(context, new Settings(), region, key);
	}

	/**
	 * Tries to find generated UI Fragment in cache by the specified key and region. If a fragment is not found in
	 * cache, generation is being invoked and generated fragment is being put in the specified cache region under the
	 * specified key.
	 *
	 * @param context  - AF context
	 * @param settings - configuration settings
	 * @param region   - cache region
	 * @param key      - key in the cache region
	 * @return UI Fragment
	 * @throws ConfigurationNotSetException     if a configuration is not set
	 * @throws TemplateFileNotFoundException    if a header or a footer file can't be found
	 * @throws InspectionException              if an inspection problem occurred, details in a message
	 * @throws AnnotationNotRegisteredException if an annotation is used, but isn't registered
	 * @throws EvaluatorException               if an evaluation problem occurred, details in a message
	 * @throws AnnotationNotFoundException      if an annotation could not be found, but it's used in a class
	 * @throws TemplateFileAccessException      if a header or a footer file is not accessible
	 * @throws CacheProviderNotSetException     if a cache provider is null
	 */
	public String generate(Context context, Settings settings, String region, String key) throws EvaluatorException,
		TemplateFileNotFoundException, InspectionException, CacheProviderNotSetException,
		AnnotationNotRegisteredException, ConfigurationNotSetException, AnnotationNotFoundException,
		TemplateFileAccessException, TagParserException {

		if (this.cacheProvider == null) {
			throw new CacheProviderNotSetException("Cache Provider is not set");
		}

		String fragment = region != null ? this.cacheProvider.get(region, key) : this.cacheProvider.get(key);
		if (fragment != null) {
			return fragment;
		}

		fragment = this.generate(context, settings);

		if (region != null) {
			this.cacheProvider.put(region, key, fragment);
		} else {
			this.cacheProvider.put(key, fragment);
		}

		return fragment;
	}

	/**
	 * Generates a UI Fragment
	 *
	 * @param context  - AF context
	 * @param settings - configuration settings
	 * @return UI Fragment as String
	 * @throws ConfigurationNotSetException     if a configuration is not set
	 * @throws TemplateFileNotFoundException    if a header or a footer file can't be found
	 * @throws InspectionException              if an inspection problem occurred, details in a message
	 * @throws AnnotationNotRegisteredException if an annotation is used, but isn't registered
	 * @throws EvaluatorException               if an evaluation problem occurred, details in a message
	 * @throws AnnotationNotFoundException      if an annotation could not be found, but it's used in a class
	 * @throws TemplateFileAccessException      if a header or a footer file is not accessible
	 */
	public String generate(Context context, Settings settings) throws ConfigurationNotSetException,
		TemplateFileNotFoundException, AnnotationNotRegisteredException, EvaluatorException,
		AnnotationNotFoundException, TemplateFileAccessException, InspectionException, TagParserException {

		if (this.configuration == null) {
			throw new ConfigurationNotSetException("Configuration is not set for AFWeaver generation");
		}

		configuration.setSettings(settings);
		context.setConfiguration(configuration);

		if (this.inspector != null) {
			this.fragmentComposer.addAllFields(this.inspector.inspect(context));
		}

		return fragmentComposer.composeForm(context).getCompleteForm().toString();
	}
}
