/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.lang.reflect.Method;

import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.util.Strings;

public class JavaMetaPropertyBuilder extends MetaPropertyBuilder {

	public JavaMetaPropertyBuilder(Context context, MetaEntity metaEntity, Method method) {
		super(context, metaEntity, getFieldName(method),
			methodReturnTypeName(method));
	}

	/**
	 * Return method return type. For all Enums is "Enum" returned. Return type
	 * "Enum" can be then used as use type in configuration and concrete Enum
	 * type can be resolved in var-guard.
	 *
	 * @param method
	 * @return method return type, for Enums return "Enum"
	 */
	public static String methodReturnTypeName(Method method) {
		if (method.getReturnType().isEnum()) {
			return Enum.class.getSimpleName();
		}
		return method.getReturnType().getSimpleName();
	}

	public static String getFieldName(Method method) {
		String field;
		if (method.getName().startsWith("get")) {
			field = method.getName().substring(3);
		} else { // isXXX
			field = method.getName().substring(2);
		}
		return Strings.lowerFirstLetter(field);
	}
}
