/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.util.HashSet;
import java.util.Set;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.annotation.AnnotationProvider;
import com.codingcrayons.aspectfaces.annotation.registration.AnnotationContainer;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.EvaluableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.OrderJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.SecurityJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.VariableJoinPoint;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.evaluation.Evaluator;
import com.codingcrayons.aspectfaces.exceptions.AnnotationNotRegisteredException;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;
import com.codingcrayons.aspectfaces.util.Arrays;
import com.codingcrayons.aspectfaces.util.Constants;
import com.codingcrayons.aspectfaces.util.Strings;

public class MetaPropertyBuilder {

	private static final double SMALL_DELTA = 0.00001;
	protected final MetaProperty metaProperty;
	protected final Context context;

	public MetaPropertyBuilder(Context context, MetaEntity metaEntity, String name, String returnType) {
		this.context = context;
		this.metaProperty = new MetaProperty(metaEntity);
		this.metaProperty.setName(name);
		this.metaProperty.setReturnType(returnType);
		this.metaProperty.setApplicable(!this.isSkipped());

		if (this.isMetaPropertyApplicable()) {
			this.setDefaultVariables();
		}
	}

	private boolean isSkipped() {
		return this.context.isContextIgnored(this.metaProperty.getName()) || !context.getConfiguration()
			.canBeFieldProcessed(metaProperty.getReturnType(), metaProperty.getName());
	}

	public final boolean isMetaPropertyApplicable() {
		return this.metaProperty.isApplicable();
	}

	public void setApplicableForEvaluable(AnnotationProvider provider) throws EvaluatorException {
		if (!isMetaPropertyApplicable()) {
			return;
		}

		EvaluableJoinPoint evaluable = AnnotationContainer.getInstance().getEvaluableAnnotation(provider.getName());
		if (evaluable != null && this.context.getConfiguration().getEvaluatorClassName() != null) {

			Evaluator evaluator;
			try {
				evaluator = (Evaluator) Class.forName(context.getConfiguration().getEvaluatorClassName()).newInstance();
			} catch (InstantiationException e) {
				throw new EvaluatorException("Evaluator " + context.getConfiguration().getEvaluatorClassName()
					+ "can not be instantiated", e);
			} catch (IllegalAccessException e) {
				throw new EvaluatorException("Evaluator " + context.getConfiguration().getEvaluatorClassName()
					+ "can not be instantiated", e);
			} catch (ClassNotFoundException e) {
				throw new EvaluatorException("Evaluator class " + context.getConfiguration().getEvaluatorClassName()
					+ " not found", e);
			}

			if (!evaluator.evaluate(evaluable.getEvaluableValue(provider))) {
				this.metaProperty.setApplicable(false);
			}
		}
	}

	public void addVariables(AnnotationProvider provider) {
		if (!isMetaPropertyApplicable()) {
			return;
		}

		VariableJoinPoint varibleJoinPoint = AnnotationContainer.getInstance().getVariableJoinPointAnnotation(
			provider.getName());
		if (varibleJoinPoint != null) {
			metaProperty.addVariables(varibleJoinPoint.getVariables(provider));
		}
	}

	private void setOrder(AnnotationProvider provider) throws AnnotationNotRegisteredException {
		if (!isMetaPropertyApplicable()) {
			return;
		}

		if (context.getOrderAnnotation() != null && context.getOrderAnnotation().equals(provider.getName())) {
			OrderJoinPoint orderable = AnnotationContainer.getInstance().getOrderableAnnotation(provider.getName());
			if (orderable == null) {
				throw new AnnotationNotRegisteredException("Annotation descriptor for " + provider.getName()
					+ " annotation is not registered!");
			}
			metaProperty.setOrder(orderable.getOrder(provider));
		}
	}

	public void setApplicableForProfiles(AnnotationProvider provider) throws AnnotationNotRegisteredException {

		if (!isMetaPropertyApplicable()) {
			return;
		}

		if (context.getProfilesAnnotation() != null && context.getProfilesAnnotation().equals(provider.getName())) {
			if (!MetaPropertyBuilder.verifyRoles(provider.getName(), provider, context.getProfiles())) {
				this.metaProperty.setApplicable(false);
			}
		}
	}

	public void setApplicableForUserRoles(AnnotationProvider provider) throws AnnotationNotRegisteredException {

		if (!isMetaPropertyApplicable()) {
			return;
		}

		if (context.getUserRolesAnnotation() != null && context.getUserRolesAnnotation().equals(provider.getName())) {
			if (!MetaPropertyBuilder.verifyRoles(provider.getName(), provider, context.getRoles())) {
				this.metaProperty.setApplicable(false);
			}
		}
	}

	public void setSpecificTag(String tag) {
		this.metaProperty.setSpecificTag(tag);
	}

	public void addAnnotation(AnnotationProvider provider) throws EvaluatorException, AnnotationNotRegisteredException {

		if (this.context.getConfiguration().isIgnoringAnnotation(provider)) {
			this.metaProperty.setApplicable(false);
		}

		this.addVariables(provider);
		this.setOrder(provider);
		this.setApplicableForEvaluable(provider);
		this.setApplicableForProfiles(provider);
		this.setApplicableForUserRoles(provider);
	}

	private static boolean verifyRoles(String annotationName, AnnotationProvider provider, String[] roles)
		throws AnnotationNotRegisteredException {

		SecurityJoinPoint roleable = AnnotationContainer.getInstance().getRoleableAnnotation(annotationName);
		if (roleable == null) {
			throw new AnnotationNotRegisteredException("Annotation descriptor for " + annotationName
				+ " annotation is not registered!");
		}
		return Arrays.intersects(roleable.getRoles(provider), roles);
	}

	/**
	 * Set default label, size, maxLength, value to field, which can be replaced by custom defined values in
	 * AFVariable. Then set variables defined in configuration, in element default.
	 * <p/>
	 * Value is set to instance
	 *
	 * @see Constants
	 * @see VariableJoinPoint
	 * @see Variable
	 */
	private void setDefaultVariables() {
		metaProperty.addVariable(new Variable(Constants.V_ENTITY_BEAN, this.metaProperty.getMetaEntity().getName()));
		metaProperty.addVariable(new Variable(Constants.V_DATA_TYPE, Strings.lowerFirstLetter(this.metaProperty.getReturnType())));
		metaProperty.addVariable(new Variable(Constants.V_LABEL, this.createLabel()));
		metaProperty.addVariable(new Variable(Constants.V_FIELD, this.metaProperty.getName()));
		metaProperty.addVariable(new Variable(Constants.V_INSTANCE, this.createInstance()));
		metaProperty.addVariable(new Variable(Constants.V_VALUE, this.createValue()));
		metaProperty.addVariable(new Variable(Constants.DEFAULT_REQUIRED, false));
		// syntax sugar
		metaProperty.addVariable(new Variable(Constants.V_CDATA_TYPE, this.metaProperty.getReturnType()));
		metaProperty.addVariable(new Variable(Constants.V_FIELDNAME, Strings.lowerFirstLetter(this.metaProperty.getName())));
		metaProperty.addVariable(new Variable(Constants.V_CFIELDNAME, Strings.upperFirstLetter(this.metaProperty.getName())));
		metaProperty.addVariable(new Variable(Constants.V_CLASSNAME, Strings.lowerFirstLetter(this.metaProperty.getMetaEntity().getSimpleName())));
		metaProperty.addVariable(new Variable(Constants.V_CCLASSNAME, Strings.upperFirstLetter(this.metaProperty.getMetaEntity().getSimpleName())));
		metaProperty.addVariable(new Variable(Constants.V_FULLCLASSNAME, this.metaProperty.getMetaEntity().getName().toLowerCase()));
		metaProperty.addVariable(new Variable(Constants.V_CFULLCLASSNAME, this.metaProperty.getMetaEntity().getName()));

		// add variables defined in configuration mapping
		metaProperty.addVariables(context.getConfiguration().getDefaultVariables(metaProperty));
	}

	/**
	 * Final method to call
	 *
	 * @return MetaProperty all before prop and after
	 */
	public Set<MetaProperty> getMetaProperties() {
		Set<MetaProperty> set = new HashSet<MetaProperty>();
		MetaProperty before = getMetaPropertyBefore();
		if (before != null) {
			set.add(before);
		}
		set.add(this.metaProperty);
		MetaProperty after = getMetaPropertyAfter();
		if (after != null) {
			set.add(after);
		}
		return set;
	}

	/**
	 * Final method to call
	 *
	 * @return MetaProperty
	 */
	public MetaProperty getMetaProperty() {
		return this.metaProperty;
	}

	/**
	 * Call once all initialized
	 *
	 * @return MetaProperty with before info
	 */
	public MetaProperty getMetaPropertyBefore() {

		Variable before = metaProperty.getVariable("before");

		if (before != null) {
			MetaProperty m = new MetaProperty(getMetaProperty().getMetaEntity(),
				"Insert", "before", getMetaProperty().getOrder() - SMALL_DELTA, null);
			m.setApplicable(true);
			m.addVariable(before);
			m.addVariable(new Variable(Constants.V_ENTITY_BEAN, this.metaProperty.getMetaEntity().getName()));
			return m;
		}
		return null;
	}

	/**
	 * Call once all initialized
	 *
	 * @return MetaProperty with after info
	 */
	public MetaProperty getMetaPropertyAfter() {

		Variable after = metaProperty.getVariable("after");

		if (after != null) {
			MetaProperty m = new MetaProperty(getMetaProperty().getMetaEntity(),
				"Insert", "after", getMetaProperty().getOrder() + SMALL_DELTA, null);
			m.setApplicable(true);
			m.addVariable(after);
			m.addVariable(new Variable(Constants.V_ENTITY_BEAN, this.metaProperty.getMetaEntity().getName()));
			return m;
		}
		return null;
	}

	/**
	 * Produces default label variable for gallery tags.
	 *
	 * @return first letter as upperCase, next upperCases separated with space
	 */
	private String createLabel() {
		StringBuilder out = new StringBuilder();
		out.append(Character.toUpperCase(this.metaProperty.getName().charAt(0)));
		for (int i = 1; i < this.metaProperty.getName().length(); ++i) {
			Character prevCh = this.metaProperty.getName().charAt(i - 1);
			Character currentCh = this.metaProperty.getName().charAt(i);
			if ((Character.isUpperCase(currentCh) && !Character.isUpperCase(prevCh)) || Character.isDigit(currentCh)) {
				out.append(" ");
			}
			out.append(currentCh);
		}
		return out.toString();
	}

	/**
	 * Produces value variable for gallery tags
	 *
	 * @return {@link #createInstance()} + "." + field name
	 * @see Constants
	 */
	private String createValue() {
		StringBuilder out = new StringBuilder();
		out.append(this.createInstance()).append(".").append(metaProperty.getName());
		return out.toString();
	}

	/**
	 * Produces instance variable for gallery tags
	 *
	 * @return AFWeaver.instanceVariablePrefix + entity simple name
	 * @see Constants
	 */
	private String createInstance() {
		return AFWeaver.getInstanceVariablePrefix() + metaProperty.getMetaEntity().getSimpleName();
	}
}
