/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation;

import java.util.HashMap;
import java.util.Map;

public class AnnotationProvider {

	private final String name;
	private final Map<String, AnnotationValueDTO<?>> values;

	public static class AnnotationValueDTO<T> {
		private final T value;

		public AnnotationValueDTO(T value) {
			this.value = value;
		}

		public T getValue() {
			return this.value;
		}

		public String getType() {
			return this.value.getClass().getName();
		}
	}

	public AnnotationProvider(String name) {
		this.name = name;
		this.values = new HashMap<String, AnnotationValueDTO<?>>(6);
	}

	public void setValue(String name, AnnotationValueDTO<?> value) {
		this.values.put(name, value);
	}

	public <T> void setValue(String name, T value) {
		this.values.put(name, new AnnotationValueDTO<T>(value));
	}

	public String getName() {
		return this.name;
	}

	public Object getValue(String key) {
		AnnotationValueDTO<?> dto = this.values.get(key);
		if (dto != null) {
			return dto.getValue();
		} else {
			return null;
		}
	}
}
