/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.util;

/**
 * Application constants
 */
public class Constants {

	public static final String EOLN = System.getProperty("separator.line", "\r\n");
	public static final String V_MAXLENGTH = "maxLength";
	public static final String V_SIZE = "size";
	public static final String V_ENTITY_BEAN_LOWER = "entityBeanLower";
	public static final String V_ENTITY_BEAN = "entityBean";
	public static final String V_DATA_TYPE_LOWER = "dataTypeLower";
	public static final String V_DATA_TYPE = "dataType";
	public static final String V_CDATA_TYPE = "DataType";
	public static final String V_INSTANCE = "instance";
	public static final String V_LABEL = "label";
	public static final String V_FIELD = "field";
	public static final String V_VALUE = "value";
	public static final String V_FRAGMENT = "fragment";
	public static final String V_FIELDNAME = "fieldName";
	public static final String V_CFIELDNAME = "FieldName";
	public static final String V_CLASSNAME = "className";
	public static final String V_CCLASSNAME = "ClassName";
	public static final String V_FULLCLASSNAME = "fullClassName";
	public static final String V_CFULLCLASSNAME = "FullClassName";
	public static final String DEFAULT_REQUIRED = "notNull";
}
