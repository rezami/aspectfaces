/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.util;

import java.util.ArrayList;
import java.util.List;

public class Collections {

	private Collections() {
	}

	/**
	 * Creates new {@link ArrayList} and adds all items from all specified lists in the specified order.
	 *
	 * @param lists - lists to concat
	 * @param <T>   - type
	 * @return created list containing all items from all specified lists
	 */
	public static <T> List<T> concatLists(List<T>... lists) {
		int size = 0;
		for (List<T> list : lists) {
			if (list != null) {
				size += list.size();
			}
		}
		List<T> resultList = new ArrayList<T>(size);
		if (size > 0) {
			for (List<T> list : lists) {
				if (list != null) {
					resultList.addAll(list);
				}
			}
		}
		return resultList;
	}

	/**
	 * Creates new {@link ArrayList} and adds all items from the specified list and all specified items (if not null) to the end.
	 *
	 * @param list  - list to concat with the specified item
	 * @param items - items to concat to the end of the specified list
	 * @param <T>   - type
	 * @return created list containing all items from the specified list and all specified items at the end of it
	 */
	public static <T> List<T> concatList(List<T> list, T... items) {
		int size = 0;
		if (list != null) {
			size += list.size();
		}
		for (T item : items) {
			if (item != null) {
				size++;
			}
		}
		List<T> resultList = new ArrayList<T>(size);
		if (size > 0) {
			if (list != null) {
				resultList.addAll(list);
			}
			for (T item : items) {
				if (item != null) {
					resultList.add(item);
				}
			}
		}
		return resultList;
	}
}
