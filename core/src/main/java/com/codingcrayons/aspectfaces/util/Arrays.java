/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.util;

public class Arrays {

	private Arrays() {
	}

	public static boolean intersects(String[] array1, String[] array2) {
		if (array1 == null && array2 == null) {
			return true;
		}
		if (array1 == null || array2 == null) {
			return false;
		}
		for (String element1 : array1) {
			for (String element2 : array2) {
				if (element1.equals(element2)) {
					return true;
				}
			}
		}
		return false;
	}

	public static String join(String[] array, String delimiter) {
		if (array == null || array.length == 0) {
			return "";
		}
		StringBuilder result = new StringBuilder(array[0]);
		for (int i = 1; i < array.length; i++) {
			result.append(delimiter).append(array[i]);
		}
		return result.toString();
	}
}
