/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.el;

import javax.el.CompositeELResolver;
import javax.el.ELContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AFCompositeELResolver extends CompositeELResolver {

	private static final Logger LOGGER = LoggerFactory.getLogger(AFCompositeELResolver.class);

	/*
	 * Returns "" for unresolved properties, no javax.el.PropertyNotFoundException is being thrown.
	 */
	public Object getValue(ELContext context, Object base, Object property) {
		Object value = super.getValue(context, base, property);
		if (!context.isPropertyResolved()) {
			LOGGER.debug("EL property {} not found", property.toString());
			context.setPropertyResolved(true);
			return "";
		}
		return value;
	}
}
