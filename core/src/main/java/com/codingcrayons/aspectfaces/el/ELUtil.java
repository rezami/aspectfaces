/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.el;

import java.util.List;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

/**
 * Utilities for work with EL
 */
public class ELUtil {

	private static ExpressionFactory factory;

	public static ValueExpression createValueExpression(AFContext elContext, String el, Class<?> returnResult) {
		if (factory == null) {
			factory = ExpressionFactory.newInstance();
		}
		return factory.createValueExpression(elContext, el, returnResult);
	}

	/*
	 * Builder pattern
	 * Builds AFContext Step by Step
	 * Responsible for setting Variables to the Context 
	 */
	private static class BasicVariableBuilder {

		private Variable variable;

		private AFContext variableContext;

		public BasicVariableBuilder() {
			variableContext = new AFContext();
		}

		final protected Variable getVariable() {
			return variable;
		}

		private boolean isValid() {
			return getVariable() != null && !getName().isEmpty();
		}

		private String getName() {
			return getVariable().getName();
		}

		protected Object getValue() {
			return getVariable().getValue();
		}

		private Class<?> getValueClass() {
			return getValue().getClass();
		}

		/**
		 * Returns built result
		 */
		public AFContext getResult() {
			return variableContext;
		}

		final public void add(Variable variable) {
			this.variable = variable;
			if (isValid()) {
				variableContext.setVariable(getName(), getValue(), getValueClass());
			}
		}
	}

	private static class ExtendedVariableBuilder extends BasicVariableBuilder {
		protected Object getValue() {
			return getVariable().getExtendedValue();
		}
	}

	/*
	 * Director : Iterates over variables and populates builder
	 */
	private static AFContext loadVariables(List<Variable> variables, BasicVariableBuilder builder) {
		for (Variable variable : variables) {
			// build part
			builder.add(variable);
		}
		// get result
		return builder.getResult();
	}

	/**
	 * Creates EL variable context of common types from the specified variables
	 *
	 * @param vars - variables to set as common values to the EL context
	 * @return created variable context
	 */
	public static AFContext makeVariableContext(List<Variable> variables) {
		return loadVariables(variables, new BasicVariableBuilder());
	}

	/**
	 * Creates EL variable context of extended variable types with special method from the specified variables.
	 *
	 * @param vars - variables to set as extended variables to the EL context
	 * @return created variable context of extended variable types
	 */
	public static AFContext makeExtendedVariableContext(List<Variable> variables) {
		return loadVariables(variables, new ExtendedVariableBuilder());
	}
}
