/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class ContextTest {

	private Context ctx;
	private Context ctxDefault;
	private final String entity = "entity";
	private final String[] profiles = {"detail", "overview"};
	private final String[] roles = {"admin", "visitor"};
	private final String layout = "customLayout.xhtml";
	private final boolean useCover = true;
	private Configuration config;

	@BeforeMethod
	public void setUp() {
		this.ctx = new Context(this.entity, this.profiles, this.roles, this.layout, this.useCover);
		this.ctxDefault = new Context(this.entity);
		this.config = new StaticConfiguration(entity);
	}

	@Test
	public void testGetConfigDefault() {
		assertNull(this.ctxDefault.getConfiguration());
	}

	@Test
	public void testSetConfig() {
		this.ctxDefault.setConfiguration(this.config);
		assertEquals(this.config, this.ctxDefault.getConfiguration());
	}

	@Test
	public void testIsUseCover() {
		assertEquals(this.useCover, this.ctx.isUseCover());
	}

	@Test
	public void testSetUseCover() {
		this.ctxDefault.setUseCover(this.useCover);
		assertEquals(this.useCover, this.ctxDefault.isUseCover());
	}

	@Test
	public void testIsUseCoverDefault() {
		assertFalse(this.ctxDefault.isUseCover());
	}

	@Test
	public void testGetLayout() {
		assertEquals(this.layout, this.ctx.getLayout());
	}

	@Test
	public void testSetLayout() {
		this.ctxDefault.setLayout(this.layout);
		assertEquals(this.layout, this.ctxDefault.getLayout());
	}

	@Test
	public void testGetLayutDefault() {
		assertNull(this.ctxDefault.getLayout());
	}

	@Test
	public void testGetRoles() {
		assertEquals(this.roles, this.ctx.getRoles());
	}

	@Test
	public void testSetRoles() {
		this.ctxDefault.setRoles(this.roles);
		assertEquals(this.roles, this.ctxDefault.getRoles());
	}

	@Test
	public void testGetRolesDefault() {
		assertNull(this.ctxDefault.getRoles());
	}

	@Test
	public void testGetProfiles() {
		assertEquals(this.profiles, this.ctx.getProfiles());
	}

	@Test
	public void testSetProfiles() {
		this.ctxDefault.setProfiles(this.profiles);
		assertEquals(this.profiles, this.ctxDefault.getProfiles());
	}

	@Test
	public void testGetProfilesDefault() {
		assertNull(this.ctxDefault.getProfiles());
	}

	@Test
	public void testGetFragmentName() {
		assertEquals(this.entity, this.ctx.getFragmentName());
	}

	@Test
	public void testSetEntity() {
		String newName = "newName";
		this.ctxDefault.setFragmentName(newName);
		assertEquals(newName, this.ctxDefault.getFragmentName());
	}

	@Test
	public void testGetEntityDefault() {
		assertEquals(this.entity, this.ctxDefault.getFragmentName());
	}

	@Test
	public void testSetDefaultOrderAnnotation() {
		String defaultOrderAnnotation = "com.foo.dummy.Annotation";
		Context.setDefaultOrderAnnotation(defaultOrderAnnotation);
		Context context = new Context();
		assertEquals(defaultOrderAnnotation, context.getOrderAnnotation());
	}

	@Test
	public void testSetDefaultProfilesAnnotation() {
		String defaultProfilesAnnotation = "com.foo.dummy.Annotation";
		Context.setDefaultProfilesAnnotation(defaultProfilesAnnotation);
		Context context = new Context();
		assertEquals(defaultProfilesAnnotation, context.getProfilesAnnotation());
	}

	@Test
	public void testSetDefaultUserRolesAnnotation() {
		String defaultUserRolesAnnotation = "com.foo.dummy.Annotation";
		Context.setDefaultUserRolesAnnotation(defaultUserRolesAnnotation);
		Context context = new Context();
		assertEquals(defaultUserRolesAnnotation, context.getUserRolesAnnotation());
	}

	@Test
	public void testGetDefaultOrderAnnotation() {
		assertEquals("com.codingcrayons.aspectfaces.annotations.UiOrder", ctxDefault.getOrderAnnotation());
	}

	@Test
	public void testGetDefaultProfilesAnnotation() {
		assertEquals("com.codingcrayons.aspectfaces.annotations.UiProfiles", ctxDefault.getProfilesAnnotation());
	}

	@Test
	public void testGetDefaultUserRolesAnnotation() {
		assertEquals("com.codingcrayons.aspectfaces.annotations.UiUserRoles", ctxDefault.getUserRolesAnnotation());
	}

	@Test
	public void testGetOrderAnnotation() {
		String annotation = "com.foo.dummy.Annotation";
		ctxDefault.setOrderAnnotation(annotation);
		assertEquals(annotation, ctxDefault.getOrderAnnotation());
	}

	@Test
	public void testGetProfilesAnnotation() {
		String annotation = "com.foo.dummy.Annotation";
		ctxDefault.setProfilesAnnotation(annotation);
		assertEquals(annotation, ctxDefault.getProfilesAnnotation());
	}

	@Test
	public void testGetUserRolesAnnotation() {
		String annotation = "com.foo.dummy.Annotation";
		ctxDefault.setUserRolesAnnotation(annotation);
		assertEquals(annotation, ctxDefault.getUserRolesAnnotation());
	}

	@Test
	public void testIsCollateDefault() {
		assertFalse(ctxDefault.isCollate());
	}

	@Test
	public void testIsCollate() {
		ctxDefault.setCollate(true);
		assertTrue(ctxDefault.isCollate());
	}
}
