/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Set;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.exceptions.AFException;
import com.codingcrayons.aspectfaces.metamodel.JavaInspector;
import com.codingcrayons.aspectfaces.support.model.Administrator;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ConfigurationTest extends TestCase {

	private final String emptyConfigurationName = "ignore-empty.config.xml";
	private final String configurationName = "ignore.config.xml";
	private final String noItemConfigurationName = "ignore-no-item.config.xml";
	private final String[] expIgnoreFields = {"version", "fullName", "password", "secret", "id"};
	private final String[] expIgnoringAnnotations = {"com.codingcrayons.aspectfaces.annotations.UiIgnore",
		"javax.persistence.Transient", "org.hibernate.validator.Email", "javax.persistence.Column",
		"org.hibernate.validator.NotNull"};

	private static final String AF_PROPS = "aspectfaces-ignore.properties";
	private static final String AF_EMPTY_PROPS = "aspectfaces-empty.properties";

	private AFWeaver afWeaver;

	@AfterClass
	@BeforeClass
	@BeforeMethod
	public void clean() {
		AFWeaver.reset();
	}

	@Test()
	public void testDefaultIgnoringAnnotations() throws AFException, FileNotFoundException {
		setUpDefaults();
		Configuration configuration = afWeaver.getConfiguration();
		Set<String> ignoringAnnotations = configuration.getIgnoringAnnotations();

		assertEquals(ignoringAnnotations.size(), 3);
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[0]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[1]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[2]));
	}

	@Test()
	public void testXMLDeclaredIgnoringAnnotations() throws AFException, FileNotFoundException {
		setUpNoDefaults();
		Configuration configuration = afWeaver.getConfiguration();
		Set<String> ignoringAnnotations = configuration.getIgnoringAnnotations();

		assertEquals(ignoringAnnotations.size(), 4);
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[0]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[2]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[3]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[4]));
	}

	@Test()
	public void testDefaultIgnoreFields() throws AFException, FileNotFoundException {
		setUpDefaults();
		Configuration configuration = afWeaver.getConfiguration();
		Set<String> ignoreFields = configuration.getIgnoreFields();

		assertEquals(ignoreFields.size(), 3);
		assertTrue(ignoreFields.contains(expIgnoreFields[0]));
		assertTrue(ignoreFields.contains(expIgnoreFields[1]));
		assertTrue(ignoreFields.contains(expIgnoreFields[4]));
	}

	@Test()
	public void testXMLDeclaredIgnoreFields() throws AFException, FileNotFoundException {
		setUpNoDefaults();
		Configuration configuration = afWeaver.getConfiguration();
		Set<String> ignoreFields = configuration.getIgnoreFields();

		assertEquals(ignoreFields.size(), 3);
		assertTrue(ignoreFields.contains(expIgnoreFields[2]));
		assertTrue(ignoreFields.contains(expIgnoreFields[0]));
		assertTrue(ignoreFields.contains(expIgnoreFields[3]));
	}

	@Test()
	public void testPermanentIgnoringAnnotations() throws AFException, FileNotFoundException {
		AFWeaver.init(new FileInputStream(getResource(AF_EMPTY_PROPS)));
		AFWeaver.addStaticConfiguration(getConfig(emptyConfigurationName));
		afWeaver = new AFWeaver(emptyConfigurationName, new JavaInspector(Administrator.class));

		Configuration configuration = afWeaver.getConfiguration();
		Set<String> ignoringAnnotations = configuration.getIgnoringAnnotations();

		assertEquals(ignoringAnnotations.size(), 1);
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[0]));
	}

	@Test()
	public void testClearedPermanentIgnoringAnnotations() throws AFException, FileNotFoundException {
		AFWeaver.init(new FileInputStream(getResource(AF_PROPS)));
		AFWeaver.addStaticConfiguration(getConfig(noItemConfigurationName));
		afWeaver = new AFWeaver(noItemConfigurationName, new JavaInspector(Administrator.class));

		Configuration configuration = afWeaver.getConfiguration();
		Set<String> ignoringAnnotations = configuration.getIgnoringAnnotations();

		assertEquals(ignoringAnnotations.size(), 3);
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[0]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[1]));
		assertTrue(ignoringAnnotations.contains(expIgnoringAnnotations[2]));
	}

	private void setUpDefaults() throws AFException, FileNotFoundException {
		AFWeaver.init(new FileInputStream(getResource(AF_PROPS)));
		AFWeaver.addStaticConfiguration(getConfig(emptyConfigurationName));
		afWeaver = new AFWeaver(emptyConfigurationName, new JavaInspector(Administrator.class));
	}

	private void setUpNoDefaults() throws AFException, FileNotFoundException {
		AFWeaver.init(new FileInputStream(getResource(AF_EMPTY_PROPS)));
		AFWeaver.addStaticConfiguration(getConfig(configurationName));
		afWeaver = new AFWeaver(configurationName, new JavaInspector(Administrator.class));
	}

	@Test()
	public void testCollidingConfigs() throws AFException, FileNotFoundException {
		// ignore : version fullName id
		// ignoreAnnotation : Transient Email
		AFWeaver.init(new FileInputStream(getResource(AF_PROPS)));
		// ignore : version fullName secret
		// ignoreAnnotation : Email Column NotNull
		AFWeaver.addStaticConfiguration(getConfig(configurationName));
		// ignore : none
		// ignoreAnnotation : empty
		AFWeaver.addStaticConfiguration(getConfig(noItemConfigurationName));
		afWeaver = new AFWeaver(configurationName, new JavaInspector(Administrator.class));
		Configuration configuration = afWeaver.getConfiguration();
		afWeaver = new AFWeaver(noItemConfigurationName, new JavaInspector(Administrator.class));
		Configuration configuration2 = afWeaver.getConfiguration();
		// expects : version fullName id secret password
		Set<String> fields = configuration.getIgnoreFields();
		// expects : UiIgnore Transient Email Column NotNull
		Set<String> annotations = configuration.getIgnoringAnnotations();
		// expects : version fullName id
		Set<String> fields2 = configuration2.getIgnoreFields();
		// expects : UiIgnore Transient Email
		Set<String> annotations2 = configuration2.getIgnoringAnnotations();

		assertEquals(fields.size(), 5);
		assertEquals(fields2.size(), 3);
		assertEquals(annotations.size(), 5);
		assertEquals(annotations2.size(), 3);
	}
}
