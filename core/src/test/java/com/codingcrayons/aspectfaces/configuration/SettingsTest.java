/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.File;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.exceptions.AFException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class SettingsTest {

	private Settings settings;

	@BeforeClass
	public void setupAF() {
		AFWeaver.reset();
	}

	@BeforeMethod
	public void setUp() {
		settings = new Settings();
	}

	@Test
	public void testGetDefaultRealPath() {
		assertNull(settings.getRealPath());
	}

	@Test(expectedExceptions = AFException.class)
	public void testGetDefaultHeaderFile() throws AFException {
		assertNull(settings.getHeaderFile());
	}

	@Test(expectedExceptions = AFException.class)
	public void testGetDefaultFooterFile() throws AFException {
		assertNull(settings.getFooterFile());
	}

	@Test
	public void testGetDefaultGalleryDirectory() {
		assertEquals(settings.getGalleryDirectory(), File.separator + "aspectfaces-gallery");
	}

	@Test
	public void testGetDefaultEvaluatorClassName() {
		assertNull(settings.getEvaluatorClassName());
	}

	@Test
	public void testGetRealPath() {
		String expected = "/tmp";
		settings.setRealPath(expected);
		assertEquals(settings.getRealPath(), expected);
	}

	@Test
	public void testGetHeaderFile() throws AFException {
		String expected = "/tmp/header.xhtml";
		settings.setHeaderFile(expected);
		assertEquals(settings.getHeaderFile(), expected);
	}

	@Test
	public void testGetFooterFile() throws AFException {
		String expected = "/tmp/footer.xhtml";
		settings.setFooterFile(expected);
		assertEquals(settings.getFooterFile(), expected);
	}

	@Test
	public void testGetGalleryDirectory() {
		String expected = "/tmp";
		settings.setGalleryDirectory(expected);
		assertEquals(settings.getGalleryDirectory(), expected);
	}

	@Test
	public void testGetEvaluatorClassName() {
		String expected = "Foo";
		settings.setEvaluatorClassName(expected);
		assertEquals(settings.getEvaluatorClassName(), expected);
	}

	@Test
	public void testSetDefaultRealPath() {
		String expected = "/tmp";
		Settings.setDefaultRealPath(expected);
		assertEquals(new Settings().getRealPath(), expected);
	}

	@Test
	public void testSetDefaultHeaderFile() throws AFException {
		String expected = "/tmp/header.xhtml";
		Settings.setDefaultHeaderFile(expected);
		assertEquals(new Settings().getHeaderFile(), expected);
	}

	@Test
	public void testSetDefaultFooterFile() throws AFException {
		String expected = "/tmp/footer.xhtml";
		Settings.setDefaultFooterFile(expected);
		assertEquals(new Settings().getFooterFile(), expected);
	}

	@Test
	public void testSetDefaultGalleryDirectory() {
		String expected = "/tmp";
		Settings.setDefaultGalleryDirectory(expected);
		assertEquals(new Settings().getGalleryDirectory(), expected);
	}

	@Test
	public void testSetDefaultEvaluatorClass() {
		String expected = "Foo";
		Settings.setDefaultEvaluatorClass(expected);
		assertEquals(new Settings().getEvaluatorClassName(), expected);
	}

	@Test
	public void testCreateSettings() throws AFException {
		String realPath = "/tmp";
		String galleryDir = "/tmp/gallery";
		String header = "/tmp/header.xhtml";
		String footer = "/tmp/footer.xhtml";
		String eval = "Foo";
		Settings settings = new Settings(realPath, galleryDir, header, footer, eval);
		assertEquals(settings.getRealPath(), realPath);
		assertEquals(settings.getHeaderFile(), header);
		assertEquals(settings.getFooterFile(), footer);
		assertEquals(settings.getEvaluatorClassName(), eval);
		assertEquals(settings.getGalleryDirectory(), galleryDir);
	}

	@AfterClass
	public void tearDown() {
		Settings.reset();
	}
}
