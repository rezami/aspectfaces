/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.plugins.saving;

import java.io.File;
import java.io.IOException;

import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;
import com.codingcrayons.aspectfaces.properties.PropertyLoader;
import com.codingcrayons.aspectfaces.util.Arrays;
import com.codingcrayons.aspectfaces.util.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Saving for saving generated UI Fragments
 */
public class SavingPlugin {

	private static final Logger LOGGER = LoggerFactory.getLogger(SavingPlugin.class);
	private static final String DEFAULT_OUTPUT_DIRECTORY;
	private static final String DEFAULT_NAME_PATTERN;
	private static final String DEFAULT_PROFILE_DELIMITER;
	private static final String DEFAULT_USER_ROLES_DELIMITER;
	private static final String PROPERTIES_FILE;

	static {
		DEFAULT_OUTPUT_DIRECTORY = "$real-path" + File.separator + "aspectfaces-output";
		DEFAULT_NAME_PATTERN = "$entity_$config_$profile_$role.xhtml";
		DEFAULT_PROFILE_DELIMITER = "-";
		DEFAULT_USER_ROLES_DELIMITER = "-";
		PROPERTIES_FILE = "/aspectfaces-saving.properties";
	}

	private String outputDirectory;
	private String namePattern;
	private String profilesDelimiter;
	private String userRolesDelimiter;

	public SavingPlugin() throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		PropertyLoader loader = new PropertyLoader(PROPERTIES_FILE);
		outputDirectory = loader.getProperty("output", DEFAULT_OUTPUT_DIRECTORY);
		namePattern = loader.getProperty("name-pattern", DEFAULT_NAME_PATTERN);
		profilesDelimiter = loader.getProperty("profiles-delimiter", DEFAULT_PROFILE_DELIMITER);
		userRolesDelimiter = loader.getProperty("user-roles-delimiter", DEFAULT_USER_ROLES_DELIMITER);
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public String getNamePattern() {
		return namePattern;
	}

	public void setNamePattern(String namePattern) {
		this.namePattern = namePattern;
	}

	public String getProfilesDelimiter() {
		return profilesDelimiter;
	}

	public void setProfilesDelimiter(String profilesDelimiter) {
		this.profilesDelimiter = profilesDelimiter;
	}

	public String getUserRolesDelimiter() {
		return userRolesDelimiter;
	}

	public void setUserRolesDelimiter(String userRolesDelimiter) {
		this.userRolesDelimiter = userRolesDelimiter;
	}

	/**
	 * @param fragment
	 * @param context  affected by AFWeaver.generate method
	 * @return absolute file path
	 * @throws java.io.IOException
	 * @see com.codingcrayons.aspectfaces.AFWeaver
	 */
	public String save(String fragment, Context context) throws IOException {
		File file = this.buildFile(context);
		Files.writeString(file, fragment);
		LOGGER.trace("Writing UI Fragment to file: '{}'.", file.getAbsolutePath());
		return file.getAbsolutePath();
	}

	private File buildFile(Context context) {

		StringBuilder path = new StringBuilder(this.outputDirectory.replace("$real-path", context
			.getConfiguration().getAbsolutePath()));
		String fileName = this.namePattern;
		fileName = fileName.replaceAll("\\$entity", context.getFragmentName());
		fileName = fileName.replaceAll("\\$config", context.getConfiguration().getName());
		fileName = fileName.replaceAll("\\$profile", Arrays.join(context.getProfiles(), this.profilesDelimiter));
		fileName = fileName.replaceAll("\\$role", Arrays.join(context.getRoles(), this.userRolesDelimiter));

		path.append(File.separator).append(fileName);

		return new File(path.toString());
	}
}
